(ns theta-star.search
  (:use [clojure.set :only (subset?)]
        [clojure.algo.monads])
  (:import [java.util PriorityQueue]))

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))

(defmacro when-not-all [conds x]
  `(when-not (and ~@conds) ~x))

(defrecord Maze [width height blocks])

(def ^:const sqrt2 (Math/sqrt 2))

(defn new-maze [p w h]
  "Makes a new maze with a block frequency of p."
  (let [blocks (set (for [i (range w)
				                  j (range h)
				                  :when (< (rand) p)]
                      [i j]))]
    (Maze. w h blocks)))

(defn blocked?
  "Tells if a square is unpassable"
  ([maze x y]
    (or (neg? x)
	      (= x (:width maze))
	      (neg? y)
	      (= y (:height maze))
	      (contains? (:blocks maze) [x y])))
  ([maze [x y]]
	  (or (neg? x)
	      (= x (:width maze))
	      (neg? y)
	      (= y (:height maze))
	      (contains? (:blocks maze) [x y]))))
    
(defn line-of-sight [^Maze maze [x0 y0] [x1 y1]]
  "Determines if there is a line of sight from [x0 y0] to [x1 y1] in maze."
  (let [dy (int (- y1 y0))
        dx (int (- x1 x0))
        sy (int (if (neg? dy) -1 1))
        sx (int (if (neg? dx) -1 1))
        dy (int (* sy dy))
        dx (int (* sx dx))
        bias-x (int (if (pos? sx) 0 -1))
        bias-y (int (if (pos? sy) 0 -1))
        x-long (boolean (>= dx dy))
        u0 (if x-long
             (int x0)
             (int y0))
        u1 (if x-long
             (int x1)
             (int y1))
        du (if x-long
             (int dx)
             (int dy))
        su (if x-long
             (int sx)
             (int sy))
        bias-u (if x-long
                 (int bias-x)
                 (int bias-y))
        v0 (if x-long
             (int y0)
             (int x0))
        v1 (if x-long
             (int y1)
             (int x1))
        dv (if x-long
             (int dy)
             (int dx))
        sv (if x-long
             (int sy)
             (int sx))
        bias-v (if x-long
                 (int bias-y)
                 (int bias-x))
        grid (if x-long
               #(blocked? maze %1 %2)
               #(blocked? maze %2 %1))]
    (loop [u0 u0
	         v0 v0
	         error (int 0)]
      (if (not= u0 u1)
        (let [error (+ error dv)
              too-much-error? (> error du)
              next-blocked? (grid (+ u0 bias-u) (+ v0 bias-v))
              branch3 (and too-much-error? (not next-blocked?))
              v0 (int (if branch3
                        (+ v0 sv)
                        v0))
              error (if branch3
                      (int (- error du))
                      (int error))]
          (if (and too-much-error? (grid (+ u0 bias-u) (+ v0 bias-v)))
            false
            (if (and (not (zero? error)) next-blocked?)
              false
              (if (and (zero? dv)
                       (grid (+ u0 bias-u)
                             v0)
                       (grid (+ u0 bias-u)
                             (- v0 1)))
                false
                (recur (int (+ u0 su))
                       v0
                       error)))))
       true))))

(defn children [maze [x y]]
  "Outputs the "
  (let [vec-add (fn [[x1 y1]] [(+ x x1) (+ y y1)])
        child-headings [[[ 1  0] 1.0 [[0 0] [0 -1]]]
                        [[ 1 -1] sqrt2 [[0 -1]]]
                        [[ 0 -1] 1.0 [[0 -1] [-1 -1]]]
                        [[-1 -1] sqrt2 [[-1 -1]]]
                        [[-1  0] 1.0 [[-1 -1] [-1 0]]]
                        [[-1  1] sqrt2 [[-1 0]]]
                        [[ 0  1] 1.0 [[-1 0] [0 0]]]
                        [[ 1  1] sqrt2 [[0 0]]]]
        all-blocked? (fn [coords]
                        (every? identity
                                (map #(blocked? maze (vec-add %1))
                                     coords)))]
    (loop [headings child-headings
           acc {}]
      (if headings
		    (let [[heading dist needs-unblocked] (first headings)
              this-blocked? (all-blocked? needs-unblocked)
		          child (vec-add heading)]
		      (if-not this-blocked?
		        (recur (next headings)
		               (assoc acc child dist))
		        (recur (next headings)
		               acc)))
        acc))))

(defn euclidean-dist [[x0 y0] [x1 y1]]
  (let [dx (- x1 x0)
        dy (- y1 y0)]
    (Math/sqrt (+ (* dx dx) (* dy dy)))))

(defn diag-manhattan-dist [[x0 y0] [x1 y1]]
  (let [dx (Math/abs (long (- x1 x0)))
        dy (Math/abs (long (- y1 y0)))]
    (+ (* sqrt2 (min dx dy)) (max dx dy) (- (min dx dy)))))

(defn a-star [child-func heuristic start goal]
  (let [fringe (PriorityQueue. 16 #(< (second %1) (second %2)))
        h (memoize heuristic)]
    (.add fringe [start (h start)])
	  (loop [g {start 0}
	         p {start nil}
           closed #{}]
      (if-let [[s s-f] (.poll fringe)]
        (if (= s goal)
          (loop [old-s s
                 path '()]
            (if old-s
              (recur (p old-s)
                     (conj path old-s))
              {:g g
               :path (vec path)
               :p p}))
	        (let [closed-2 (conj closed s)
                [new-g new-p] (loop [children-list (seq (child-func s))
				                             g-acc g
				                             p-acc p]
				                        (if children-list
				                          (let [[child cost] (first children-list)]
					                          (if (and (not (contains? closed-2 child))
						                                 (or (not (g child))
						                                     (< (+ (g s) cost) (g child))))
					                            (do (.remove fringe child) 
							                            (.add fringe [child (+ (g s) cost (h child))])
											                    (recur (next children-list)
					                                       (assoc g-acc child (+ (g s) cost))
				                                         (assoc p-acc child s)))
					                            (recur (next children-list)
					                                   g-acc
				                                     p-acc)))
				                          [g-acc p-acc]))
                       #_(doall (for [[child cost] (child-func s)
							                      :when (and (not (contains? closed-2 child))
							                                 (or (not (g child))
							                                     (< (+ (g s) cost) (g child))))]
                                (do (.remove fringe child) 
			                              (.add fringe [child (+ (g s) cost (h child))])
							                      [child (+ (g s) cost)])))]
	          (recur new-g
	                 new-p
                   closed-2)))
        false))))

(defn theta-star [maze heuristic start goal]
  (let [fringe (PriorityQueue. 16 #(< (second %1) (second %2)))
        h (memoize heuristic)
        child-func #(children maze %)]
    (.add fringe [start (h start)])
	  (loop [g {start 0}
	         p {start start}
           closed #{}]
      (if-let [[s s-f] (.poll fringe)]
        (if (= s goal)
          (loop [old-s s
                 path '()]
            (if (not= old-s start)
                  (recur (p old-s)
                         (conj path old-s))
              {:g g
               :path (vec (conj path old-s))
               :p p}))
	        (let [closed-2 (conj closed s)
                [new-g new-p] (loop [children-list (seq (child-func s))
				                             g-acc g
				                             p-acc p]
				                        (if children-list
				                          (let [[child immediate-cost] (first children-list)
                                        parent #_(loop [s s
                                                      i 0]
                                                 (let [p-s (p s)]
	                                                 (if (and (line-of-sight maze child p-s)
                                                            (not= s p-s))  
	                                                   (recur p-s (inc i))
                                                     s)))
				                                        (let [p-s (p s)]
                                                  (if (line-of-sight maze child p-s)
                                                    p-s
                                                    s))
                                        cost (if (not= parent s)
                                               (euclidean-dist parent child)
                                               immediate-cost)]
					                          (if (and (not (contains? closed-2 child))
						                                 (or (not (g child))
						                                     (< (+ (g parent) cost) (g child))))
					                            (do (.remove fringe child) 
							                            (.add fringe [child (+ (g parent) cost (h child))])
											                    (recur (next children-list)
					                                       (assoc g-acc child (+ (g parent) cost))
				                                         (assoc p-acc child parent)))
					                            (recur (next children-list)
					                                   g-acc
				                                     p-acc)))
				                          [g-acc p-acc])) 
                #_(doall (for [[child immediate-cost] (child-func s)
                                    :let [gp? (line-of-sight maze child (p s))
                                          parent (if gp?
                                                     (p s)
                                                     s)
                                          cost (euclidean-dist parent child)]
							                      :when (and (not (contains? closed-2 child))
							                                 (or (not (g child))
							                                     (< (+ (g parent) cost) (g child))))]
                                (do (.remove fringe child) 
			                              (.add fringe [child (+ (g parent) cost (h child))])
							                      [child parent (+ (g parent) cost)])))]
           #_(doall (for [[child cost] (child-func s)
							                      :when (and (not (contains? closed-2 child))
							                                 (or (not (g child))
							                                     (< (+ (g s) cost) (g child))))]
                                (do (.remove fringe child) 
			                              (.add fringe [child (+ (g s) cost (h child))])
							                      [child (+ (g s) cost)])))
	          (recur new-g
	                 new-p
                   closed-2)))
        false))))

(defn visibility-children [maze goal [x y]]
  (loop [blocks (conj (:blocks maze) goal)
         visible {}]
    (if blocks
	    (let [ul (first blocks)
	          ur [(inc (first ul)) (second ul)]
	          lr [(inc (first ul)) (inc (second ul))]
	          ll [(first ul) (inc (second ul))]
	          w-ul (if (line-of-sight maze ul [x y])
	                 (assoc visible ul (euclidean-dist [x y] ul))
	                 visible)
	          w-ur (if (line-of-sight maze ur [x y])
	                 (assoc w-ul ur (euclidean-dist [x y] ur))
	                 w-ul)
	          w-lr (if (line-of-sight maze lr [x y])
	                 (assoc w-ur lr (euclidean-dist [x y] lr))
	                 w-ur)
	          w-ll (if (line-of-sight maze ll [x y])
	                 (assoc w-lr ll (euclidean-dist [x y] ll))
	                 w-lr)]
	      (recur (next blocks)
	             w-ll))
     visible)))