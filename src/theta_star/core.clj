(ns theta-star.core
  (:use [seesaw core graphics]
        [clojure.algo.monads :only [domonad maybe-m]]
        theta-star.search)
  (:import (java.awt Dimension)
           (java.awt.event MouseEvent))
  (:gen-class))

; Width and height in tiles
(def map-w 100)
(def map-h 50)
; The width and height of tiles
(def tile-w 10)
(def tile-h 10)
; Probability of tiles
(def tile-p 0.1)

(set! *unchecked-math* true)

; The visual width and height in pixels
(def win-w (* map-w tile-w))
(def win-h (* map-h tile-h))

; Initial values for path and maze stuff
(def ^:dynamic *maze* (ref #{}))
(def ^:dynamic *maze-path* (ref []))
(def ^:dynamic *maze-end* (ref nil))
(def ^:dynamic *g* (ref {}))
(def ^:dynamic *h* (ref {}))
(def ^:dynamic *p* (ref {}))
(def ^:dynamic *drawable* nil)


(defn random-vertex []
  "Makes a random vertex"
  [(rand-int (inc map-w)) (rand-int (inc map-h))])

(defn pixel-scale [x y]
  [(* x tile-w) (* y tile-h)])

(defn draw-point [g c x y]
  "Draws a point at a vertex"
  (let [point-style (style :background c)
        half-tile-dims [(/ tile-w 2) (/ tile-h 2)]
        [pixel-x pixel-y] (map - (pixel-scale x y) half-tile-dims)]
    (draw g
          (ellipse pixel-x pixel-y tile-w tile-h) point-style)))

(defn draw-block [g x y & {:keys [color] :or [color :black]}]
  "Draws a block to the lower-right of a vertex"
  (let [block-style (style :background color)
        [ul-x ul-y] (pixel-scale x y)]
    (draw g
          (rect ul-x ul-y tile-w tile-h) block-style)))

(defn draw-lines [g coords]
  "Connects a series of points with lines"
  (let [pixel-coords (map #(apply pixel-scale %) coords)
        line-style (style :foreground :blue
                          :stroke 1)
        start-end-pairs (map vector pixel-coords (rest pixel-coords))
        lines (for [[[x1 y1] [x2 y2]] start-end-pairs]
                (line x1 y1 x2 y2))
        line-style-pairs (interleave lines (repeat line-style))]
    #_(doall (for [[start end] (map vector coords (rest coords))]
               (println start end (debug-line-of-sight g (deref *maze*) start end))))
    (apply draw g line-style-pairs)))

(defn draw-maze [g]
  "Draws the maze"
  (doall
	  (for [[x y] (:blocks (deref *maze*))]
	    (draw-block g x y :color :black))))

(defn draw-path [g]
  "Draws the path"
  (draw-lines g (deref *maze-path*)))

(defn test-sight [g [x0 y0]]
  "Draws a path to every point"
  (let [maze (deref *maze*)]
  (doall (for [x1 (range (* -2 (inc map-w)) (* 3 (inc map-w)))
               y1 (range (* -2 (inc map-h)) (* 3 (inc map-w)))
               :when (line-of-sight maze [x0 y0] [x1 y1])]
           (draw-lines g [[x0 y0] [x1 y1]])))))

(defn draw-stuff [g]
  "Draws the graphical components"
  (dosync
	  (draw-path g)
    #_(when (deref *maze*)
      (test-sight g [(/ map-w 2) (/ map-h 2)]))
	  (draw-maze g)
	  (when (seq (deref *maze-path*))
	    (apply draw-point g :red (first (deref *maze-path*))))
	  (when (deref *maze-end*)
	    (apply draw-point g :lime (deref *maze-end*)))
    ))

(defn reset-maze! [p]
  "Sets a new random maze and clears the path info."
  (dosync
	  (ref-set *maze* (new-maze tile-p map-w map-h))
	  (ref-set *maze-end* (random-vertex))
	  (ref-set *maze-path* [(random-vertex) (deref *maze-end*)])
    (repaint! p)))

(defn change-ends! [p which-end coord]
  (dosync
    (case which-end
      :end (ref-set *maze-end* coord)
      :start (ref-set *maze-path* [coord (deref *maze-end*)]))
    (repaint! p)))

(defn solve-maze! [p algo child-func h]
  "Accepts a panel to draw on, an algorithm to solve with, a child function
  to describe the graph to be searched, and the heuristic to use while
  searching, finds a path through the graph, and draws it."
  (dosync
    (let [start (first (deref *maze-path*))
          goal (deref *maze-end*)
          h #(h goal %)
          results (if (= algo theta-star)
                    (theta-star (deref *maze*) h start goal)
                    (algo child-func h start goal))
          new-path (:path results)
          g (:g results)]
      (ref-set *maze-path* new-path)
      (ref-set *g* g)
      (ref-set *h* h)
      (ref-set *p* (:p results))
      (repaint! p))))
 
(defn content-panel []
  (let [stat-label (label :id :stats)
        show-tile-stats (fn [^MouseEvent e]
                          (let [loc (.getPoint e)
	                              pix-x (.x loc)
	                              pix-y (.y loc)
	                              map-x (int (/ (+ pix-x (/ tile-w 2)) tile-w))
	                              map-y (int (/ (+ pix-y (/ tile-h 2)) tile-h))
	                              round #(format "%.2f" (double %1))
	                              g ((deref *g*) [map-x map-y])
	                              g-text (if g
	                                       (str "g(n) = " (round g) "  ")
	                                       "")
	                              h ((deref *h*) [map-x map-y])
	                              h-text (if h
	                                       (str "h(n) = " (round h) "  ")
	                                       "")
	                              f (when (and g h) (+ g h))
	                              f-text (if f
	                                       (str "f(n) = " (round f) "  ")
	                                       "")
	                              p ((deref *p*) [map-x map-y])
	                              p-text (if p
	                                       (str "p(n) = " (vec p) "  ")
	                                       "")]
	                      (config! stat-label :text
	                               (str "Location: (" map-x ", " map-y ")  "
	                                    g-text
	                                    h-text
	                                    f-text
	                                    p-text))))
        click-handler (fn [^MouseEvent e]
                        (let [loc (.getPoint e)
                              pix-x (.x loc)
                              pix-y (.y loc)
                              map-x (int (/ (+ pix-x (/ tile-w 2)) tile-w))
                              map-y (int (/ (+ pix-y (/ tile-h 2)) tile-h))
                              p (.getSource e)]
                          (if (= (.getButton e) MouseEvent/BUTTON1)
                            (change-ends! p :start [map-x map-y])
                            (change-ends! p :end [map-x map-y]))))
                            
                        
        maze-panel (canvas
                     :id :map
                     :background :white
                     :paint (fn [c g] (draw-stuff g))
                     :listen [:mouse-moved show-tile-stats])
        new-maze (fn [e] (reset-maze! maze-panel))
        run-a-star (fn [e] (solve-maze! maze-panel
                                        a-star
                                        #(children (deref *maze*) %)
                                        diag-manhattan-dist))
        run-theta-star (fn [e] (solve-maze! maze-panel
                                            theta-star
                                            nil
                                            euclidean-dist))
        run-visibility-graph (fn [e] (solve-maze! maze-panel
                                            a-star
                                            #(visibility-children
                                               (deref *maze*)
                                               (deref *maze-end*)
                                               %)
                                            euclidean-dist))
        buttons [(button :text "New Maze"
                         :listen [:action new-maze])
                 (button :text "Grid A*"
                         :listen [:action run-a-star])
                 (button :text "Θ*"
                         :listen [:action run-theta-star])
                 (button :text "Visibility A*"
                         :listen [:action run-visibility-graph])
                 stat-label]]
    (doto maze-panel
		  (.setMinimumSize (Dimension. win-w win-h))
		  (.setSize win-w win-h)
		  (.setPreferredSize (Dimension. win-w win-h)))
		(border-panel
		  :center maze-panel
		  :south (flow-panel :items buttons))))

(defn make-frame []
  (let [f (frame :title "A* and Θ*"
                 :on-close :dispose
                 :visible? true
                 :resizable? false
                 :content (content-panel))]
    (pack! f)
    (show! f)))

(defn benchmark [p w h iters]
  (dotimes [n iters]
    (let [maze (new-maze p w h)
          start [(rand-int (inc w)) (rand-int (inc h))]
          goal [(rand-int (inc w)) (rand-int (inc h))]]
          (theta-star maze #(euclidean-dist goal %) start goal)
          #_(a-star #(children maze %) #(diag-manhattan-dist goal %) start goal) )))

(defn length-test [p w h iters]
  (loop [n iters
         a-star-length (double 0.0)
         theta-star-length (double 0.0)
         visibility-length (double 0.0)
         a-star-h (double 0.0)
         theta-star-h (double 0.0)
         visibility-h (double 0.0)]
    (if-not (neg? n)
		  (let    [maze (new-maze p w h)
	 		         start [(rand-int (inc w)) (rand-int (inc h))]
			         goal [(rand-int (inc w)) (rand-int (inc h))]
			         theta-star-results (theta-star maze #(euclidean-dist goal %) start goal)
			         a-star-results (a-star #(children maze %) #(diag-manhattan-dist goal %) start goal)
			         visibility-results (a-star #(visibility-children
                                             maze
                                             goal
                                             %)
                                          #(euclidean-dist goal %)
                                          start
                                          goal)]
       (if a-star-results
         (recur (dec n)
                (+ a-star-length ((:g a-star-results) goal))
                (+ theta-star-length ((:g theta-star-results) goal))
                (+ visibility-length ((:g visibility-results) goal))
                (+ a-star-h (diag-manhattan-dist goal start))
                (+ theta-star-h (euclidean-dist goal start))
                (+ visibility-h (euclidean-dist goal start)))
		     (recur n
                a-star-length
                theta-star-length
                visibility-length
                a-star-h
                theta-star-h
                visibility-h)))
      {:a-star {:length (/ a-star-length iters), :h (/ a-star-h iters)},
       :theta-star {:length (/ theta-star-length iters), :h (/ theta-star-h iters)}
       :visibility {:length (/ visibility-length iters), :h (/ visibility-h iters)}})))
  
(defn -main [& args]
  (native!)
  (make-frame))