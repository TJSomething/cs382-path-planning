#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble

\end_preamble
\use_default_options false
\begin_modules
theorems-starred
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman lmodern
\font_sans lmss
\font_typewriter lmtt
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize letterpaper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
CS 382: Path Finding
\end_layout

\begin_layout Author
Thomas Kelly
\end_layout

\begin_layout Date
March 1, 2012
\end_layout

\begin_layout Section
Optimization
\end_layout

\begin_layout Standard
Due to my choice of language (Clojure), there were many low hanging fruit.
 Many of the optimizations were also applicable to Java code, as Clojure
 runs on the JVM.
 Benchmarking was done by running each function 1,000 times on 1,000 different
 randomly-generated mazes of size 
\begin_inset Formula $100\times50$
\end_inset

 on an Intel Core i7-2500K.
 All runtimes are averages based on these benchmarks.
\end_layout

\begin_layout Standard
Unoptimized, the code ran A* in 28.3 ms and 
\begin_inset Formula $\Theta$
\end_inset

* in 50.3 ms.
 Profiling revealed that slow downs were caused by storing the maze and
 its attributes in a hashmap.
 Since I did not need to add or remove things from this hashmap, I converted
 it into a class.
 This sped A* up to 14.4 ms and 
\begin_inset Formula $\Theta$
\end_inset

* to 25.6 ms.
 Other optimizations included: making the vector addition function used
 to calculate adjacent squares only add a single vector to the current location,
 putting possible directions to into vectors
\begin_inset Marginal
status open

\begin_layout Plain Layout
Clojure vectors are similar in nature to STL vectors in that they have constant
 access time.
\end_layout

\end_inset

 instead of hashmaps, using manual tail recursion instead of various built-in
 functions to accumulate results, and replacing a call to my line-of-sight
 function in my children function with a lookup table.
 With all of these, A* ran in 2.9 ms (
\begin_inset Formula $9.8\times$
\end_inset

 faster) and 
\begin_inset Formula $\Theta$
\end_inset

* ran in 4.8 ms (
\begin_inset Formula $10.4\times$
\end_inset

 faster).
 I suspect that if I converted all of the vectors into Java arrays, unboxed
 all of my variables, and type hinted all parameters, I could see further
 improvements.
 The lack of improvement in 
\begin_inset Formula $\Theta$
\end_inset

* is due to the fact that 
\begin_inset Formula $\Theta$
\end_inset

* as originally written did not give optimal paths.
\end_layout

\begin_layout Section
Comparison
\end_layout

\begin_layout Standard
The runtimes of A* and 
\begin_inset Formula $\Theta$
\end_inset

* are noted above.
 The resulting path lengths are close, but 
\begin_inset Formula $\Theta$
\end_inset

* is consistently shorter.
 In an average of 5,000 trials, A* yielded path lengths of 43.0 units long,
 while 
\begin_inset Formula $\Theta$
\end_inset

* achieved lengths of 40.9.
 In comparison, the average heuristic result for start to finish for A*
 and 
\begin_inset Formula $\Theta$
\end_inset

*, respectively, was 42.9 and 40.7.
\end_layout

\begin_layout Standard
The heuristics provided for A* and 
\begin_inset Formula $\Theta$
\end_inset

* are both good when used with the corresponding algorithm.
 For A*, in most trials, the heuristic provides the actual distance.
 With 
\begin_inset Formula $\Theta$
\end_inset

*, this is not the case.
 However, in both cases, the average distance is within 1% of heuristic,
 which implies that the heuristics provided are pretty good.
 I suspect that faster heuristics with constant runtime do not exist.
\end_layout

\begin_layout Section
Visibility Graph Comparison
\end_layout

\begin_layout Standard
I implemented the visibility path algorithm using A* on by modifying the
 children function and using a Euclidean heuristic.
 In 10,000 trials, the average path lengths for 
\begin_inset Formula $\Theta$
\end_inset

* and the A* on the visibility graph were 40.907 and 40.830, respectively.
 This shows a 0.19% improvement by using A* on the visibility graph over
 
\begin_inset Formula $\Theta$
\end_inset

*.
\end_layout

\begin_layout Section
Proof that A* with the Given Heuristic Gives Optimal Solutions
\end_layout

\begin_layout Standard
Let's assume that the given heuristic is not consistent.
 Then, there exists a pair of adjacent nodes, 
\begin_inset Formula $s$
\end_inset

 and 
\begin_inset Formula $s^{\prime}$
\end_inset

, such that
\begin_inset Formula 
\[
h\left(s\right)>c\left(s,s^{\prime}\right)+h\left(s^{\prime}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
On the grid that is navigated on, the cost between neighbors is either 1
 or 
\begin_inset Formula $\sqrt{2}$
\end_inset

.
\end_layout

\begin_layout Standard
If the cost between 
\begin_inset Formula $s$
\end_inset

 and 
\begin_inset Formula $s^{\prime}$
\end_inset

 is 
\begin_inset Formula $\sqrt{2}$
\end_inset

, then 
\begin_inset Formula $h\left(s\right)>\sqrt{2}+h\left(s^{\prime}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
Since 
\begin_inset Formula 
\[
h\left(s\right)=\sqrt{2}\cdot\min\left(\left|s^{x}-s_{goal}^{x}\right|,\left|s^{y}-s_{goal}^{y}\right|\right)+\max\left(\left|s^{x}-s_{goal}^{x}\right|,\left|s^{y}-s_{goal}^{y}\right|\right)-\min\left(\left|s^{x}-s_{goal}^{x}\right|,\left|s^{y}-s_{goal}^{y}\right|\right)
\]

\end_inset

and 
\begin_inset Formula $s^{\prime}=\left(s^{x}\pm1,s^{y}\pm1\right)$
\end_inset

, then then changing 
\begin_inset Formula $s$
\end_inset

 to 
\begin_inset Formula $s^{\prime}$
\end_inset

 can only yield a difference in the heuristic of 
\begin_inset Formula $\pm\sqrt{2}$
\end_inset

, as both 
\begin_inset Formula $s^{x}$
\end_inset

 and 
\begin_inset Formula $s^{y}$
\end_inset

 change between 
\begin_inset Formula $s$
\end_inset

 and 
\begin_inset Formula $s^{\prime}$
\end_inset

.
 That is, 
\begin_inset Formula $h\left(s\right)\in\left\{ h\left(s^{\prime}\right)-\sqrt{2},h\left(s^{\prime}\right)+\sqrt{2}\right\} $
\end_inset

.
 Therefore,
\begin_inset Formula 
\begin{align*}
h\left(s\right) & \leq h\left(s^{\prime}\right)+\sqrt{2}\\
\sqrt{2}+h\left(s^{\prime}\right) & <h\left(s\right)\leq h\left(s^{\prime}\right)+\sqrt{2}\\
\sqrt{2}+h\left(s^{\prime}\right) & <h\left(s^{\prime}\right)+\sqrt{2}\\
\sqrt{2}+h\left(s^{\prime}\right) & \neq h\left(s^{\prime}\right)+\sqrt{2}
\end{align*}

\end_inset

This is a contradiction, so 
\begin_inset Formula $c\left(s,s^{\prime}\right)$
\end_inset

 cannot be 
\begin_inset Formula $\sqrt{2}$
\end_inset

.
\end_layout

\begin_layout Standard
If we assume that the 
\begin_inset Formula $c\left(s,s^{\prime}\right)=1$
\end_inset

, then only one of the coordinates change between 
\begin_inset Formula $s$
\end_inset

 and 
\begin_inset Formula $s^{\prime}$
\end_inset

, so 
\begin_inset Formula $h\left(s\right)-h\left(s^{\prime}\right)=\pm1$
\end_inset

.
 So,
\begin_inset Formula 
\begin{align*}
h\left(s\right) & \leq h\left(s^{\prime}\right)+1\\
c\left(s,s^{\prime}\right)+h\left(s^{\prime}\right) & <h\left(s\right)\\
1+h\left(s^{\prime}\right) & <h\left(s\right)\leq h\left(s^{\prime}\right)+1\\
1+h\left(s^{\prime}\right) & <h\left(s^{\prime}\right)+1\\
1+h\left(s^{\prime}\right) & \neq h\left(s^{\prime}\right)+1
\end{align*}

\end_inset

This is also a contradiction, so 
\begin_inset Formula $c\left(s,s^{\prime}\right)\neq1$
\end_inset

.
\end_layout

\begin_layout Standard
However, since 
\begin_inset Formula $c\left(s,s^{\prime}\right)$
\end_inset

 must be either 
\begin_inset Formula $1$
\end_inset

 or 
\begin_inset Formula $\sqrt{2}$
\end_inset

, then an earlier assumption must be false.
 Therefore, the heuristic is consistent.
\end_layout

\begin_layout Standard
Since the given heuristic is consistent, A* will give an optimal solution.
\end_layout

\begin_layout Section
Proof That 
\begin_inset Formula $\Theta$
\end_inset

* is not Optimal
\end_layout

\end_body
\end_document
